const config = require('config');
const seneca = require('seneca')({log: 'silent'}).client({port: config.get('todo_port'), host: config.get('todo_host')});

exports.resolver = {
    Query: {
        getAllTodo: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'todo', cmd: 'getAll', accessToken: accessToken}, function(err, res){
                    if (err) console.log('getAllTodo: ', err);
                    else {
                        resolve(res.result);
                    };
                });
            });
        },
        getTodo: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'todo', cmd: 'get', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('getTodo:', err);
                    else resolve(res.result);
                });
            });
        }
    },
    Mutation: {
        addTodo: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'todo', cmd: 'add', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('addTodo: ', err);
                    else resolve({
                        status: 'success',
                        data: res.result.message
                    });
                });
            });
        },
        updateTodo: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'todo', cmd: 'update', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('updteTodo: ', err);
                    else resolve({
                        status: 'success',
                        data: res.result.message
                    });
                });
            });
        },
        deleteTodo: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'todo', cmd: 'delete', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('deleteTodo: ', err);
                    else resolve({
                        status: res.result.status,
                        data: res.result.message
                    });
                });
            });
        }
    }
}