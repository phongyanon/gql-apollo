query getAllTodo{
  getAllTodo {
    id
    name
    description
    date
  }
}

query getTodo{
  getTodo(id: "Todo/341378"){
    id
    name
    description
    date
  }
}

mutation addTodo{
  addTodo(name: "orn", description: "so cute", date: "2019-06-03"){
    status
    data
  }
}

mutation updateTodo{
  updateTodo(id: "Todo/341378", name: "orn"){
    status
    data
  }
}

mutation deleteTodo{
  deleteTodo(id: "Todo/341467"){
    status
    data
  }
}