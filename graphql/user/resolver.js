const config = require('config');
const seneca = require('seneca')({log: 'silent'}).client({port: config.get('user_port'), host: config.get('user_host')});
const request = require('request');

const oauth2_url = `${config.get('oauth2_host')}:${config.get('oauth2_port')}/auth/token`

exports.resolver = {
    Query: {
        getByUsername: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'user', login: true, cmd: 'getByUsername', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('getByUsername:', err);
                    else resolve(res.result);
                });
            });
        },
        getUser: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'user', login: true, cmd: 'get', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('getUser:', err);
                    else resolve(res.result);
                });
            });
        }
    },
    Mutation: {
        login: (root, args, context, info) => {
            return new Promise((resolve) => {
                // console.log('oauth2: ', oauth2_url, args);
                let options = {
                    method: 'POST',
                    url: oauth2_url,
                    headers: {
                        'Authorization': `Basic ${config.get('todo_auth')}`,
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    form: {
                        grant_type: 'password',
                        username: args.username.toString(),
                        password: args.password.toString()
                    }
                }
                request(options, function(err, result){
                    if(err) console.log('login: ', err);
                    else {
                        let token = JSON.parse(result.body);
                        resolve({
                            accessToken: token.accessToken,
                            accessTokenExpiresAt: token.accessTokenExpiresAt,
                            refreshToken: token.refreshToken,
                            refreshTokenExpiresAt: token.refreshTokenExpiresAt,
                        }); 
                    }            
                });
            });
        },
        getNewTokens: (root, args, context, info) => {
            let refreshToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                let options = {
                    method: 'POST',
                    url: oauth2_url,
                    headers: {
                        'Authorization': `Basic ${config.get('todo_auth')}`,
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    form: {
                        grant_type: 'refresh_token',
                        refresh_token: refreshToken
                    }
                }
                request(options, function(err, result){
                    console.log('getNewTokens: ', result.body)
                    if(err) console.log('login: ', err);
                    else {
                        let token = JSON.parse(result.body);
                        resolve({
                            accessToken: token.accessToken,
                            accessTokenExpiresAt: token.accessTokenExpiresAt,
                            refreshToken: token.refreshToken,
                            refreshTokenExpiresAt: token.refreshTokenExpiresAt,
                        }); 
                    }            
                });
            });
        },
        register: (root, args, context, info) => {
            return new Promise((resolve) => {
                args.user_type_id = 2; //TODO: fix user type is user.
                seneca.act({ms: 'user', cmd: 'register', data: args}, function(err, res){
                    if (err) console.log('register: ', err);
                    else resolve({
                        status: 'success',
                        data: res.result.message
                    });
                });
            });
        },
        deleteUser: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'user', login: true, cmd: 'delete', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('deleteUser: ', err);
                    else resolve({
                        status: 'success',
                        data: res.result.message
                    });
                });
            });
        }
    }
}