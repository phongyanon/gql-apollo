const { ApolloServer, gql } = require('apollo-server');
const glue = require('schemaglue');
const config = require('config');
const port = config.get('port');

const options = {
    js: '**/*.js',
    ignore: '**/somefileyoudonotwant.js'
}
const { schema, resolver } = glue('graphql', options);
const typeDefs = gql(schema);
const resolvers = resolver;

const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: ({ req }) => { // verify token in oath2 server instead.
        const token = req.headers.authorization || '';
        // console.log('context: ', token);
        // const  user = verify_token(token);
        // if (!user) throw new AuthorizationError('you must be logged in'); 
        return { auth: 'auth user', token: token };
    }
});

server.listen({port: port}).then(({url}) => {
    console.log(`🚀  Server ready at ${url}`);
});

/**
 * TODO List
 * -/ update edit api of user and todo to update only specific field.
 * -/ connect apollo with user and todo without authen.  ***do login in oauth2-server
 * - create integration test in point of apollo client. https://www.apollographql.com/docs/apollo-server/features/testing/
 * - seperate environment auth and no auth for development and test.
 * - integrate authen system to apollo
 */