const books = [
    {
        id: 1,
        title: 'Harry Potter and the Chamber of Secrets',
        author: 'J.K. Rowling',
    },
    {
        id: 2,
        title: 'Jurassic Park',
        author: 'Michael Crichton',
    },
    {
        id: 3,
        title: 'Star Wars',
        author: 'Josh Lucus',
    },
] // mockup data

let dbID = books.length + 1;

exports.resolver = {
    Query: {
        // can use seneca here.
        books: () => books,
        getBook: (root, args, context, info) => {
            let id = args.id;
            return books.filter(obj => {
                return obj.id == id;
            })[0];
        }
    },
    Mutation: {
        updateBook: (root, args, context, info) => {
            console.log('updateBook: ', context);
            let id = args.id;
            for(i in books){
                let obj = books[i];
                if(obj.id === id){
                    obj.title = args.title;
                    obj.author = args.author;
                    return {status: `Updated! ${obj.title}`};            
                }
            }
            return {status: 'Failed to update'};
        },
        addBook: (root, args, context, info) => {
            let newBook = {
                id: dbID,
                title: args.title,
                author: args.author
            };
            books.push(newBook);
            dbID++;
            return newBook;
        },
        deleteBook: (root, args, context, info) => {
            let id = args.id;
            for(i in books){
                let obj = books[i];
                if(obj.id === id){
                    books.splice(i, 1);
                    return {status: 'Successfully delete'}
                }
            }
            return {status: 'Failed to delete'};
        }
    }
}